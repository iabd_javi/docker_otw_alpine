# Utiliza una imagen base de Alpine Linux
FROM alpine:latest

# Instala las herramientas necesarias (openssh-client, sshpass, git) en Alpine Linux
RUN apk update && apk add openssh-client sshpass && apk add git

# Clonar repositorio y si existe, bórralo
RUN rm -rf docker_otw_alpine
RUN git clone https://gitlab.com/iabd_javi/docker_otw_alpine.git

# Cambiar al directorio /docker_otw_alpine
WORKDIR /docker_otw_alpine

# Concede permisos de ejecución al script
RUN chmod +x level1-7_alpine.sh

# Ejecuta el script
CMD ["bash level1-7_alpine.sh"]
